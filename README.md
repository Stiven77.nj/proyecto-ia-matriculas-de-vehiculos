# Reconocimiento de matriculas de vehiculos

<img src="https://gitlab.com/Stiven77.nj/proyecto-ia-matriculas-de-vehiculos/-/raw/main/Banner_-_PROYECTO_IA.png"  width="800px" height="300px">


## Autores

Maria Paula Rodriguez, Helman Andres Merchan Quevedo, Nicolas Stiven Jaimes

## Objetivo

Clasificar imágenes de vehículos según si presentan matrículas o no, y extraer de una imagen la matrícula de un carro (en formato texto)

## DataSet

https://drive.google.com/drive/folders/1cm7BmdCoGr1XTPwLZYMd7HTa6T-aDTLR?usp=sharing

## Metodo de clasificacion implementados

- Gaussian NB
- RandomForestClassifier
- DecisionTreeClassifier
- SuportVectorClassifier
- Red Neuronal Profunda
- Red Neuronal Convoluciona

## Video

https://youtu.be/H5gCQNH9YSY


